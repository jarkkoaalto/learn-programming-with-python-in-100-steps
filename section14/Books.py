class Book:
    def __init__(self,name, copies = 0):
        self.name= name
        self.copies = copies
        
    def increase_copies(self, howmuch):
        self.copies += howmuch

    def decrease_copies(self, howmuch): 
        self.copies -= howmuch

the_art_of_computer_programming = Book('The art of computer programming')
learning_python = Book('Learning Python in 100 steps', 30)
learning_restful_services = Book('Learning Restful Service in Depth')
        

   # print(the_art_of_computer_programming.name)
        
learning_python.increase_copies(25)
learning_python.decrease_copies(24)
print(learning_python.copies)