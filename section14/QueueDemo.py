'''
Created on 10.10.2018

@author: Jarkko
'''

import datetime
s = datetime.datetime.today()
print(s)

today_date = datetime.datetime.today()
print(today_date)

datetime.datetime(2018,5,21,10,0,39,732463)
print(today_date.year)
print(today_date.month)
print(today_date.hour)
print(today_date.minute)
print(today_date.second)

some_date = datetime.datetime(2019,5,27)
print(some_date)

f = some_date.date()
print(f)

day = some_date
v = day + datetime.timedelta(2033,7,5,5,7,2,234567)
print(v)