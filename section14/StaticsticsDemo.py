'''
Created on 10.10.2018

@author: Jarkko
'''
import statistics

marks = [1,6,8,34,6]
print(statistics.mean(marks))
print(statistics.median(marks))
print(statistics.median_low(marks))
print(statistics.median_high(marks))
