'''
Created on 10.10.2018

@author: Jarkko
'''
def example_method(mandatory_parameter, default_parameter="Default",*args,**kwargs):
    print(f"""
    mandatory_parameter = {mandatory_parameter}
    default_parameter = {default_parameter}
    args = {args}
    kwargs = {kwargs}
    """)
    
   # example_method(mandatory_parameter=15)
example_method(25,45)
example_method(25,"String 1","String 2","String 3", key1='a1',key2='v3')