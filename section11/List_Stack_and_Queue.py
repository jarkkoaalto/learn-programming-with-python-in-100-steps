numbers = []
numbers.append(1)
numbers.append(2)
numbers.append(3)
numbers.append(4)
numbers.append(5)
numbers.append(6)
numbers.pop()

print(numbers)

numbers.pop()
print(numbers)

numbers.append(10)
numbers.pop()
print(numbers)

numbers.pop(0)

print(numbers)
