
class Student:
    def __init__(self, name, marks):
        self.name = name
        self.marks = marks
        
    def get_number_of_marks(self):
        return len(self.marks)
    
    def determine_maximum_mark(self):
        return max(self.marks)
    
    def determine_minimum_mark(self):
        return min(self.marks)
    
    def determine_average(self):
        return average(self.marks)
    
    def get_total_sum_of_marks(self):
        return self.average(self.marks)
    
    def remove_mark_at_index(self, index):
        del self.marks[index]
    
student = Student("Ranga",[23,45,56,67])
number = student.get_number_of_marks()
sum_of_average = student.get_total_sum_of_marks()
maximum_mark = student.determine_maximum_mark()
minimum_mark = student.determine_minimum_mark()
average = student.determine_average()
student.add_new_marks(35)
student.remove_mark_at_index(2)
print(f"""Student[number_of_marks-{number} 
    average{student.get_total_sum_of_marks}
     max-{maximum_mark} 
     min-{minimum_mark} 
     avg-{average} ]""")
