from test._test_multiprocessing import sqr
str = "Rhis is an awesome occasion. This has never happened before."

sqr_first_ten_numbers = [i*i for i in range(1,11)]
print(type(sqr_first_ten_numbers))

sqr_first_ten_numbers_set = set(sqr_first_ten_numbers)

print(type(sqr_first_ten_numbers_set))

sqrt_first_ten_numbers_dict = {i:i*i for i in range(1,11)}
print(type(sqrt_first_ten_numbers_dict))

print(sqrt_first_ten_numbers_dict)
print(type([]))
print(type(set()))
print(type({}))
print(type({1}))
print(type({'A',5}))
print(type(()))