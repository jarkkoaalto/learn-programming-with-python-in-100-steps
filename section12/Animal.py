'''
Created on 7.10.2018

@author: Jarkko
'''
from abc import ABC, abstractmethod

class AbstracitAnimal(ABC):
    @abstractmethod
    def bark(self): pass
    
class Dog(AbstracitAnimal):
    def bark(self):
        print("Bow Bow")
        
Dog().bark()