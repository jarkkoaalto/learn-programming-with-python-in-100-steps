'''
Created on 7.10.2018

@author: Jarkko
'''
from abc import ABC, abstractclassmethod, abstractmethod
# Repeare - rawmaterial
# recipe
# cleanup

class AbstractionRecipe(ABC):
    
    def execute(self):
        self.repare()
        self.recipe()
        self.cleanup()
        
    @abstractmethod
    def repare(self): pass
    
    @abstractmethod
    def recipe(self): pass
    
    @abstractmethod
    def cleanup(self): pass
    
class MicrowaveRecipe(AbstractionRecipe):
    def prepare(self):
        print("do the dishes")
        print("get raw materials")
        print("switch on microwavr")
        
    def recipe(self):
        print("execute the steps")
        
    def cleanup(self): 
        print("Switch off microwave")
        
MicrowaveRecipe().execute()