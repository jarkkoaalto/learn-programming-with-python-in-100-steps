'''
Created on 7.10.2018

@author: Jarkko
'''

class Book:
    def __init__(self,id,name,author):
        self.id = id
        self.name = name
        self.author = author
        self.reviews = []
        
    def __repr__(self):
        return repr((self.id, self.name, self.author, self.reviews))
    
    def add_review(self, review):
        self.reviews.append(review)
    
class Review:
    def __init__(self,id,desription, rating):
        self.id = id
        self.desription = desription
        self.rating = rating
        
        
    def __repr__(self):
        return repr((self.id, self.desription, self.rating))
    
        
book = Book(123,"Object oriented Programming with Python","Ranga" )

review1 = Review(10,"Great book",5)
book.add_review(review1)
book.add_review(Review(100,"Awesome",6))
print(book)
