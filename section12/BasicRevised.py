'''
Created on 7.10.2018

@author: Jarkko
'''
class MotorBike:
    def __init__(self, speed):
        self.speed = speed # State
        
    def increase_speed(self, how_much):
        self.speed += how_much
        
        
    # Behavior
    def decrease_speed(self, how_much):
        if(self.speed-how_much>0):
            self.speed -= how_much
        else:
            print("Get a life")
            
    
# increase 1 or object 1
honda = MotorBike(50)

#instance 3 0r object 2
ducati = MotorBike(250)

honda.increase_speed(50)
honda.decrease_speed(100)

print(honda.speed)
print(ducati.speed)