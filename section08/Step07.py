

def print_square_upto_limit(limit):
    i = 1
    while(i*i < limit):
        print(i*i, end=" ")
        i = i+1

print_square_upto_limit(30)

print()
def print_cubers_upto_limit(limit):
    j = 1
    while (j * j * j < limit):
        print(j * j * j, end=" ")
        j = j + 1


print_cubers_upto_limit(30)