def calculate_sum_of_divisors(number):

    sum = 0
    if(number < 2):
        return sum

    for divisor in range(1, number +1 ):
        if number % divisor == 0:
            sum = sum + divisor

    return sum

print(calculate_sum_of_divisors(6))
print(calculate_sum_of_divisors(15))