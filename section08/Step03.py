def is_prime(number):

    if(number < 2):
        return False

    for divisor in range(2,number):
        if number % divisor == 0:
            return False

    return True


print(is_prime(23))

# Sum_upto_n(6)
# sum of numbers upto n
# 1+2+3+4+5+6

def sum_upto_n(number):
    sum = 0

    for i in range(1, number+1):
        sum = sum + 1

    return sum

print(sum_upto_n(6))
print(sum_upto_n(10))