# is_prime(9); // Is a number prime?
# H: 5 => true, 7 => true, 11 => true 6=> false

def is_prime(number):
    if (number < 2):
        return False

    # 2,3,4
    for divisor in range(2,number):
        if number % divisor == 0:
            return False

    return True

print(is_prime(7))

print(is_prime(71))
print(is_prime(17))
print(is_prime(2))
