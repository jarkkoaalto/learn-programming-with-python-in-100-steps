def product_of_two_numbers(a,b):
    print(a*b)

product_of_two_numbers(3,5)
product_of_two_numbers(23,3)

product = product_of_two_numbers(1,2)
print(product)

maximus = max(1,2,3,4,5)

print(maximus * 5)

def two_numbers(a,b):
    product = a * b
    return product

print(two_numbers(3,4))


def three_numbers(a,b,c):
    sum = a+b+c
    return sum

something = three_numbers(1,2,3)
print(something)

def calculate_third_angle(first,second):
    return 180 - (first+second)

print(calculate_third_angle(50,20))