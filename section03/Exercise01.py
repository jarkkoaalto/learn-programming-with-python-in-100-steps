# Exersices
# Methods - basics

# 1. Write and execute a method name say_hello_world_thrice
#   to print hello world trice-
# 2. Write and execute a method that prints the following four statement:
#   - I've createed my first variable
#   - I've created my first loop
#   - I've created my first method
#   - I'm exited to learn Python

def say_hello_world_thrice():
    print("hello, world !")
    print("hello, world !")
    print("hello, world !")

print(say_hello_world_thrice())


def four_statements():
    print("I've created my first variable")
    print("I've created my first loop")
    print("I've created my first method")
    print("I'm excited to learn Python")

print(four_statements())