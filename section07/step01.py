message = "Hello world"
print(message)
print(type(message))

# Uppercase
print(message.upper())
# Lowercase
print(message.lower())

# Capitalize
print(message.capitalize())

# Also
print("hello world".capitalize())

print('hello'.islower())

print('Hello'.capitalize())

print('hello'.isupper())

print("HELLO".isupper())

print('123'.isdigit())

print('23'.isalpha())

print('abc'.isalpha())

print('ABC 123'.isalnum())

print('Hello world'.endswith('old'))
print('Hello world'.endswith('orl'))
print('Hello world'.startswith('Hello'))
print('Hello world'.find('world'))

