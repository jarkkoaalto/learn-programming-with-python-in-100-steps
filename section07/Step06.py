import string
char = 'a'
vowel_string = 'aeiouäöAEIUÄÖ'

print(char.upper() in vowel_string)

print(string.ascii_uppercase)

for u in string.ascii_uppercase:
    print(u)


for y in string.ascii_lowercase:
    print(y)


for m in string.digits:
    print(m)


print('b'.lower() in vowel_string)