import string

s = string.ascii_letters
print(s)
l = string.ascii_lowercase
print(l)

st = string.digits
print(st)

u = string.punctuation
print(u)

print('a' in string.ascii_letters)
