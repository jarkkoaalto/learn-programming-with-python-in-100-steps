i = 5

if i > 3:
    print(f"{i} is greater than 3")

a = 5
b = 7

if(a<b):
    print("a is greater than b")


a = 1
b = 2
c = 3
d = 5
if(a+b > c+d ):
    print("a+b > c+d")
else:
    print("a+b < c+d")


angle1 = 30
angle2 = 20
angle3 = 60
if (angle1 + angle2 + angle3 == 180):
    print("Valid Triangle")
else:
    print("Invalid Triangle")

angle1 = 90
angle2 = 30
angle3 = 60

if(angle1 + angle2 + angle3 == 180):
    print("Valid Triangle")