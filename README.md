# Learn Programming with Python in-100 Steps

About this course:
Python 3 Tutorial using Hands-on, Step By Step Approach. Learn Programming with 100+ code examples.
Course content
### Section 01: Introduction
### Section 02: Introduction To Python Programming With Multiplication Table
### Section 03: Introduction To Methods - Multiplication Table
### Section 04: Introduction To Python Platform
### Section 05: Introduction To PyCharm
### Section 06: Basic Numeric Data Types and Conditional Execution
### Section 07: Text in Python
### Section 08: Python Loops
### Section 09: Python Tips For Beginners
### Section 10: Introduction To Object Oriented Programming
### Section 11: Python Data Structures
### Section 12: Object Oriented Programming Again
### Section 13: Error Handling with Python
### Section 14: A Few More Python Tips
