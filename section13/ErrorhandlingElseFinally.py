'''
Created on 9.10.2018

@author: Jarkko
'''
# Open file/recource

try:
    # Business logic to read
    i = 1 # not hardcoded, getting a input feom user
    j =10 /i
    values = [1,'1']
    sum(values)
except TypeError:
    print("TypeError")
    j = 10
except ZeroDivisionError:
    print("ZeroDivisionError")
    j = 0
else:
    print("Else")
finally:
    # Close 
    print("Finally")
    
print(j)
print("End")