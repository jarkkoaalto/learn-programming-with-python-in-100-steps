'''
Created on 9.10.2018

@author: Jarkko
'''

try:
    i = 1 # not hardcoded, getting a input from user
    j = 10/i
    values = [1,2]
    sum(values)
except TypeError:
    print("Typeerror")
    j = 10
except ZeroDivisionError:
    print("ZeroDivisionError")
    j = 0

print(j)
print("End")